# dark_gtk_linux
Dark themes, mostly (but not entirely) based off of the dark Solarized template.  GTK2 GTK3 Openbox, Tint2, DoubleCommander

![screenshot](screenshot.png?raw=true "Screenshot")


GTK themes:  [Adwaita-Dark](https://www.gnome-look.org/content/show.php/?content=148170)  
DoubleCommander: Figured it out myself. CUT AND PASTE from the doublecmd.xml file.  
Tint2: Drop in.  
Xresources: Rename to .Xresources.  It's not solarized so much as dark. Still works well.  
Firefox/Iceweasel: [GLOBAL Dark Style: Wombat Style](https://userstyles.org/styles/130029/global-dark-style-wombat-style-firefox-mozilla)  
Openbox3: Currently from crunchy-dark-grey http://crunchbang.org/forums/viewtopic.php?id=31510 ; plan to make more solarized soon.  
